package name.lech.project.calculator;

public class CalculationFailedException extends RuntimeException {

    private CalculationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a {@link CalculationFailedException} with a message indicating an unknown error.
     */
    public static CalculationFailedException unknown() {
        return unknown(null);
    }

    /**
     * Constructs a {@link CalculationFailedException} with a message indicating an unknown error.
     */
    public static CalculationFailedException unknown(Throwable cause) {
        return new CalculationFailedException("Calculation failed due to an unknown error", cause);
    }

    /**
     * Constructs a {@link CalculationFailedException} with a message indicating validation errors.
     */
    public static CalculationFailedException validation(Throwable cause) {
        return new CalculationFailedException("Calculation failed due to validation errors", cause);
    }

    /**
     * Constructs a {@link CalculationFailedException} with a message indicating that used units are not equal.
     */
    public static CalculationFailedException unitsNotEqual(Throwable cause) {
        return new CalculationFailedException("Cannot perform operations on different units. Convert them to a common unit first.", cause);
    }

    /**
     * Constructs a {@link CalculationFailedException} with a message indicating that an arithmetic exception has occurred.
     */
    public static CalculationFailedException arithmetic(Throwable cause) {
        return new CalculationFailedException("Calculation failed due to an arithmetic exception", cause);
    }
}
