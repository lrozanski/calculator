package name.lech.project.calculator;

import name.lech.project.calculator.operation.*;
import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.UnitValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Calculator {

    private Operations operations;

    @Autowired
    public Calculator(Operations operations) {
        this.operations = operations;
    }

    /**
     * Returns the result of addition of multiple numbers.
     *
     * @throws CalculationFailedException thrown when the calculation fails for any reason
     */
    public ValueWithUnit add(Unit asUnit, ValueWithUnit... values) {
        return performOperation(new Addition(), asUnit, values);
    }

    /**
     * Returns the result of subtraction of multiple numbers.
     *
     * @throws CalculationFailedException thrown when the calculation fails for any reason
     */
    public ValueWithUnit subtract(Unit asUnit, ValueWithUnit... values) {
        return performOperation(new Subtraction(), asUnit, values);
    }

    /**
     * Returns the result of multiplication of multiple numbers.
     *
     * @throws CalculationFailedException thrown when the calculation fails for any reason
     */
    public ValueWithUnit multiply(Unit asUnit, ValueWithUnit... values) {
        return performOperation(new Multiplication(), asUnit, values);
    }

    /**
     * Returns the result of division of multiple numbers.
     *
     * @throws CalculationFailedException thrown when the calculation fails for any reason
     */
    public ValueWithUnit divide(Unit asUnit, ValueWithUnit... values) {
        return performOperation(new Division(), asUnit, values);
    }

    private ValueWithUnit performOperation(Operation operation, Unit asUnit, ValueWithUnit[] values) {
        try {
            return operations.performOperation(operation, asUnit, values);
        } catch (UnitValidationException e) {
            throw CalculationFailedException.validation(e);
        } catch (ArithmeticException e) {
            throw CalculationFailedException.arithmetic(e);
        } catch (Exception e) {
            throw CalculationFailedException.unknown(e);
        }
    }
}
