package name.lech.project.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class CalculatorApplication {

    public static void main(String[] args) {
        if (args.length > 0) {
            new SpringApplicationBuilder()
                .web(WebApplicationType.NONE)
                .profiles("command-line")
                .sources(CalculatorApplication.class)
                .run(args);

            return;
        }
        SpringApplication.run(CalculatorApplication.class, args);
    }
}
