package name.lech.project.calculator;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import name.lech.project.calculator.unit.Unit;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.text.DecimalFormat;

public class ValueWithUnit {

    public static final DecimalFormat DECIMAL_FORMAT = createDecimalFormat();

    @NotNull
    private final BigDecimal value;

    @NotNull
    private final Unit unit;

    @JsonCreator
    public ValueWithUnit(
        @JsonProperty("value") @NotNull BigDecimal value,
        @JsonProperty("unit") @NotNull Unit unit
    ) {
        this.value = value;
        this.unit = unit;
    }

    private static DecimalFormat createDecimalFormat() {
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMinimumFractionDigits(0);
        decimalFormat.setMaximumFractionDigits(20);

        return decimalFormat;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Unit getUnit() {
        return unit;
    }

    /**
     * @see Unit#requireExpected(Unit)
     */
    public void requireUnit(Unit expected) {
        unit.requireExpected(expected);
    }

    @Override
    public String toString() {
        return DECIMAL_FORMAT.format(value) + " " + unit.toString().toLowerCase();
    }
}
