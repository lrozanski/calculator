package name.lech.project.calculator.cli;

import name.lech.project.calculator.unit.Unit;
import org.springframework.stereotype.Component;

@Component
public class ArgumentParser {

    /**
     * Tries to parse a {@link Unit} from the provided string.
     *
     * @throws IllegalArgumentException thrown if an invalid unit is provided
     */
    Unit parseUnit(String unitString) {
        switch (unitString.toLowerCase()) {
            case "meters":
                return Unit.METERS;
            case "feet":
                return Unit.FEET;
            case "nautical miles":
                return Unit.NAUTICAL_MILES;
            default:
                throw new IllegalArgumentException();
        }
    }
}
