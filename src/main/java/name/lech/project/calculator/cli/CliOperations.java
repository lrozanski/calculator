package name.lech.project.calculator.cli;

import name.lech.project.calculator.Calculator;
import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CliOperations {

    private Calculator calculator;

    @Autowired
    public CliOperations(Calculator calculator) {
        this.calculator = calculator;
    }

    /**
     * Performs any supported CLI operation and prints the result to System.out.
     *
     * @throws UnsupportedOperationException thrown if an unsupported operator is used
     */
    @SuppressWarnings("squid:S106")
    void perform(ValueWithUnit first, ValueWithUnit second, String operator, Unit asUnit) {
        switch (operator) {
            case "+":
                System.out.println(calculator.add(asUnit, first, second));
                break;
            case "-":
                System.out.println(calculator.subtract(asUnit, first, second));
                break;
            case "*":
                System.out.println(calculator.multiply(asUnit, first, second));
                break;
            case "/":
                System.out.println(calculator.divide(asUnit, first, second));
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
