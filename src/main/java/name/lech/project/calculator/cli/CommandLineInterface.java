package name.lech.project.calculator.cli;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Profile("command-line")
public class CommandLineInterface implements CommandLineRunner {

    private static final String UNITS = "meters|feet|nautical\\smiles";
    private static final String CALCULATOR_PATTERN = "([0-9.]+) (" + UNITS + ") ([+\\-*/]) ([0-9.]+) (" + UNITS + ") as (" + UNITS + ")";

    private CliOperations operations;
    private ArgumentParser argumentParser;
    private HelpPrinter helpPrinter;

    @Autowired
    public CommandLineInterface(CliOperations operations, ArgumentParser argumentParser, HelpPrinter helpPrinter) {
        this.operations = operations;
        this.argumentParser = argumentParser;
        this.helpPrinter = helpPrinter;
    }

    @Override
    @SuppressWarnings("squid:S106")
    public void run(String... args) {
        for (String arg : args) {
            Matcher matcher = Pattern
                .compile(CALCULATOR_PATTERN)
                .matcher(arg);

            if (!matcher.find()) {
                helpPrinter.print();
                break;
            }
            System.out.print(arg + ": ");
            handleOperation(matcher);
        }
    }

    private void handleOperation(Matcher matcher) {
        ValueWithUnit first = new ValueWithUnit(
            new BigDecimal(matcher.group(1)),
            argumentParser.parseUnit(matcher.group(2))
        );
        ValueWithUnit second = new ValueWithUnit(
            new BigDecimal(matcher.group(4)),
            argumentParser.parseUnit(matcher.group(5))
        );
        String operator = matcher.group(3);
        Unit asUnit = argumentParser.parseUnit(matcher.group(6));

        operations.perform(first, second, operator, asUnit);
    }
}
