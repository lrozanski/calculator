package name.lech.project.calculator.cli;

import org.springframework.stereotype.Component;

@Component
public class HelpPrinter {

    /**
     * Prints command line help.
     */
    @SuppressWarnings("squid:S106")
    void print() {
        System.out.println("Invalid pattern");
        System.out.println("Usage:");
        System.out.println("\t<value> <unit> <operator> <value> <unit> as <unit>");
        System.out.println();
        System.out.println("\twhere <unit> is one of: meters, feet, nautical miles");
        System.out.println("\twhere <operator> is one of: + - * /");
    }
}
