package name.lech.project.calculator.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import name.lech.project.calculator.ValueWithUnit;

import java.io.IOException;

class ValueWithUnitSerializer extends StdSerializer<ValueWithUnit> {

    ValueWithUnitSerializer() {
        super(ValueWithUnit.class);
    }

    @Override
    public void serialize(ValueWithUnit value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        String formattedValue = ValueWithUnit.DECIMAL_FORMAT.format(value.getValue());

        gen.writeStartObject();
        gen.writeFieldName("value");
        gen.writeString(formattedValue);
        gen.writeFieldName("asUnit");
        gen.writeString(value.getUnit().name());
        gen.writeEndObject();
    }
}
