package name.lech.project.calculator.operation;

import name.lech.project.calculator.ValueWithUnit;

import javax.validation.constraints.NotNull;
import java.math.RoundingMode;

public class Division implements Operation {

    /**
     * Divides both values and returns the result.
     */
    @NotNull
    @Override
    public ValueWithUnit perform(@NotNull ValueWithUnit first, @NotNull ValueWithUnit other) {
        validateArguments(first, other);

        return new ValueWithUnit(
            first.getValue().divide(
                other.getValue(),
                findMaxScale(first, other) + 1,
                RoundingMode.HALF_UP
            ),
            first.getUnit()
        );
    }

    private int findMaxScale(@NotNull ValueWithUnit first, @NotNull ValueWithUnit other) {
        return Math.max(
            first.getValue().scale(),
            other.getValue().scale()
        );
    }
}
