package name.lech.project.calculator.operation;

import name.lech.project.calculator.ValueWithUnit;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@FunctionalInterface
public interface Operation {

    /**
     * Performs an operation using both arguments.
     */
    @NotNull
    ValueWithUnit perform(@NotNull ValueWithUnit first, @NotNull ValueWithUnit other);

    /**
     * Checks if arguments are not null and units match each other.
     */
    default void validateArguments(ValueWithUnit first, ValueWithUnit other) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(other);

        first.getUnit().validateEqual(other.getUnit());
    }
}
