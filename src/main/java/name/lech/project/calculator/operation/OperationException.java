package name.lech.project.calculator.operation;

public class OperationException extends RuntimeException {

    private OperationException(String message) {
        super(message);
    }

    public static OperationException notEnoughValues() {
        return new OperationException("Not enough values provided");
    }
}
