package name.lech.project.calculator.operation;

import name.lech.project.calculator.CalculationFailedException;
import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.conversion.UnitConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;

@Component
public class Operations {

    private UnitConverter unitConverter;

    @Autowired
    public Operations(UnitConverter unitConverter) {
        this.unitConverter = unitConverter;
    }

    public ValueWithUnit performOperation(Operation operation, Unit asUnit, ValueWithUnit... values) {
        Objects.requireNonNull(operation);
        Objects.requireNonNull(asUnit);
        Objects.requireNonNull(values);

        if (values.length < 2) {
            throw OperationException.notEnoughValues();
        }
        return Arrays
            .stream(values)
            .map(value -> unitConverter.convert(value, asUnit))
            .reduce(operation::perform)
            .map(value -> unitConverter.convert(value, asUnit))
            .orElseThrow(CalculationFailedException::unknown);
    }
}
