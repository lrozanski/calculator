package name.lech.project.calculator.operation;

import name.lech.project.calculator.ValueWithUnit;

import javax.validation.constraints.NotNull;

public class Subtraction implements Operation {

    /**
     * Subtracts both values and returns the result.
     */
    @NotNull
    @Override
    public ValueWithUnit perform(@NotNull ValueWithUnit first, @NotNull ValueWithUnit other) {
        validateArguments(first, other);

        return new ValueWithUnit(
            first.getValue().subtract(other.getValue()),
            first.getUnit()
        );
    }
}
