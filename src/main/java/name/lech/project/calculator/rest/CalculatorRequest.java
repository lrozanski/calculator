package name.lech.project.calculator.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CalculatorRequest {

    @Valid
    @NotNull
    private final List<@Valid ValueWithUnit> values;

    @NotNull
    private final Unit asUnit;

    @JsonCreator
    public CalculatorRequest(
        @JsonProperty("values") @NotNull List<ValueWithUnit> values,
        @JsonProperty("asUnit") @NotNull Unit asUnit
    ) {
        this.values = values;
        this.asUnit = asUnit;
    }

    public List<ValueWithUnit> getValues() {
        return values;
    }

    public Unit getAsUnit() {
        return asUnit;
    }
}
