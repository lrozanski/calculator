package name.lech.project.calculator.rest;

import name.lech.project.calculator.Calculator;
import name.lech.project.calculator.ValueWithUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@CrossOrigin
@RestController
@RequestMapping(
    value = "/",
    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class CalculatorResource {

    private Calculator calculator;

    @Autowired
    public CalculatorResource(Calculator calculator) {
        this.calculator = calculator;
    }

    @PostMapping("/add")
    ResponseEntity<ValueWithUnit> add(@RequestBody @NotNull @Valid CalculatorRequest request) {
        ValueWithUnit result = calculator.add(
            request.getAsUnit(),
            request.getValues().toArray(new ValueWithUnit[0])
        );
        return ResponseEntity.ok(result);
    }

    @PostMapping("/subtract")
    ResponseEntity<ValueWithUnit> subtract(@RequestBody @NotNull @Valid CalculatorRequest request) {
        ValueWithUnit result = calculator.subtract(
            request.getAsUnit(),
            request.getValues().toArray(new ValueWithUnit[0])
        );
        return ResponseEntity.ok(result);
    }

    @PostMapping("/multiply")
    ResponseEntity<ValueWithUnit> multiply(@RequestBody @NotNull @Valid CalculatorRequest request) {
        ValueWithUnit result = calculator.multiply(
            request.getAsUnit(),
            request.getValues().toArray(new ValueWithUnit[0])
        );
        return ResponseEntity.ok(result);
    }

    @PostMapping("/divide")
    ResponseEntity<ValueWithUnit> divide(@RequestBody @NotNull @Valid CalculatorRequest request) {
        ValueWithUnit result = calculator.divide(
            request.getAsUnit(),
            request.getValues().toArray(new ValueWithUnit[0])
        );
        return ResponseEntity.ok(result);
    }
}
