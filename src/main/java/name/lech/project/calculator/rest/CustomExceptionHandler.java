package name.lech.project.calculator.rest;

import name.lech.project.calculator.CalculationFailedException;
import name.lech.project.calculator.operation.OperationException;
import name.lech.project.calculator.unit.UnitValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler({CalculationFailedException.class})
    public ResponseEntity<ErrorResponse> handleCalculationFailedException(CalculationFailedException exception) {
        ErrorResponse response = new ErrorResponse(
            formatMessage(exception),
            findCause(exception)
        );
        HttpStatus responseStatus = resolveStatus(
            findRootCause(exception)
        );

        LOGGER.error(exception.getMessage(), exception);
        return ResponseEntity
            .status(responseStatus)
            .body(response);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> handleCalculationFailedException(MethodArgumentNotValidException exception) {
        ErrorResponse response = new ErrorResponse(
            formatMessage(exception),
            findCause(exception)
        );
        HttpStatus responseStatus = resolveStatus(
            findRootCause(exception)
        );

        return ResponseEntity
            .status(responseStatus)
            .body(response);
    }

    private HttpStatus resolveStatus(Throwable cause) {
        if (cause instanceof CalculationFailedException
            || cause instanceof OperationException
            || cause instanceof UnitValidationException) {
            return HttpStatus.BAD_REQUEST;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponse> handleCalculationFailedException(Exception exception) {
        ErrorResponse response = new ErrorResponse(
            formatMessage(exception),
            findCause(exception)
        );
        LOGGER.error(exception.getMessage(), exception);
        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(response);
    }

    private String formatMessage(Throwable exception) {
        return String.format(
            "%s: %s",
            exception.getClass().getCanonicalName(),
            exception.getMessage()
        );
    }

    private String findCause(Exception exception) {
        Throwable rootCause = findRootCause(exception);
        return rootCause != null
            ? formatMessage(rootCause)
            : null;
    }

    private Throwable findRootCause(Exception exception) {
        Throwable rootCause = NestedExceptionUtils.getRootCause(exception.getCause());
        return rootCause == null
            ? exception.getCause()
            : rootCause;
    }
}
