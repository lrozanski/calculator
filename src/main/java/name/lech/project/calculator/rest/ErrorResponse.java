package name.lech.project.calculator.rest;

public class ErrorResponse {

    private final String errorMessage;
    private final String cause;

    public ErrorResponse(String errorMessage, String cause) {
        this.errorMessage = errorMessage;
        this.cause = cause;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getCause() {
        return cause;
    }
}
