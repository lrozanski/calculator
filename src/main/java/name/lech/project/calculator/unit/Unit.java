package name.lech.project.calculator.unit;

import java.util.Objects;
import java.util.function.Supplier;

public enum Unit {
    METERS,
    FEET,
    NAUTICAL_MILES;

    /**
     * Throws {@link UnitValidationException} if the value is not equal to the provided argument.
     */
    public void validateEqual(Unit other) {
        requireSame(
            other,
            () -> UnitValidationException.notEqual(this, other)
        );
    }

    /**
     * Throws {@link UnitValidationException} if the current value is different from expected.
     */
    public void requireExpected(Unit expected) {
        requireSame(
            expected,
            () -> UnitValidationException.unexpected(this, expected)
        );
    }

    private void requireSame(Unit other, Supplier<UnitValidationException> exceptionSupplier) {
        Objects.requireNonNull(other);
        Objects.requireNonNull(exceptionSupplier);

        if (this != other) {
            throw exceptionSupplier.get();
        }
    }
}
