package name.lech.project.calculator.unit;

public class UnitValidationException extends RuntimeException {

    private UnitValidationException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link UnitValidationException} with a message indicating that values are not equal.
     */
    public static UnitValidationException notEqual(Unit first, Unit other) {
        return new UnitValidationException("Units " + first.name() + " and " + other.name() + " are not equal");
    }

    /**
     * Constructs a {@link UnitValidationException} with a message indicating that the current value is unexpected.
     */
    public static UnitValidationException unexpected(Unit current, Unit expected) {
        return new UnitValidationException("Unit " + current.name() + " is different from expected: " + expected.name());
    }
}
