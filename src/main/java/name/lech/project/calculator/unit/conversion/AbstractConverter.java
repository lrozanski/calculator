package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;

import java.util.Objects;

abstract class AbstractConverter {

    /**
     * The base unit of this converter.
     */
    abstract Unit baseUnitType();

    /**
     * Converts the value to other units.
     *
     * @throws NullPointerException thrown if any of the arguments is null
     */
    ValueWithUnit toUnit(ValueWithUnit value, Unit unit) {
        Objects.requireNonNull(value);
        Objects.requireNonNull(unit);
        value.requireUnit(baseUnitType());

        switch (unit) {
            case METERS:
                return toMeters(value);
            case FEET:
                return toFeet(value);
            case NAUTICAL_MILES:
                return toNauticalMiles(value);
            default:
                throw new UnsupportedOperationException();
        }
    }

    /**
     * Converts the value to meters.
     */
    protected abstract ValueWithUnit toMeters(ValueWithUnit value);

    /**
     * Converts the value to feet.
     */
    protected abstract ValueWithUnit toFeet(ValueWithUnit value);

    /**
     * Converts the value to nautical miles.
     */
    protected abstract ValueWithUnit toNauticalMiles(ValueWithUnit valueWithUnit);
}
