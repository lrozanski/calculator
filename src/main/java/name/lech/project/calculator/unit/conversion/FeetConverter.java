package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
class FeetConverter extends AbstractConverter {

    private static final BigDecimal TO_METERS_MULTIPLIER = new BigDecimal("0.3048");
    private static final BigDecimal TO_NAUTICAL_MILES_MULTIPLIER = new BigDecimal("0.00016457883");

    @Override
    Unit baseUnitType() {
        return Unit.FEET;
    }

    @Override
    protected ValueWithUnit toMeters(ValueWithUnit value) {
        return new ValueWithUnit(
            value.getValue().multiply(TO_METERS_MULTIPLIER),
            Unit.METERS
        );
    }

    @Override
    protected ValueWithUnit toFeet(ValueWithUnit value) {
        return value;
    }

    @Override
    protected ValueWithUnit toNauticalMiles(ValueWithUnit value) {
        return new ValueWithUnit(
            value.getValue().multiply(TO_NAUTICAL_MILES_MULTIPLIER),
            Unit.NAUTICAL_MILES
        );
    }
}
