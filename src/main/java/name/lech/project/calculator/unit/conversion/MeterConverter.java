package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
class MeterConverter extends AbstractConverter {

    private static final BigDecimal TO_FEET_MULTIPLIER = new BigDecimal("3.2808399");
    private static final BigDecimal TO_NAUTICAL_MILES_MULTIPLIER = new BigDecimal("0.00053995680");

    @Override
    Unit baseUnitType() {
        return Unit.METERS;
    }

    @Override
    protected ValueWithUnit toMeters(ValueWithUnit value) {
        return value;
    }

    @Override
    protected ValueWithUnit toFeet(ValueWithUnit value) {
        return new ValueWithUnit(
            value.getValue().multiply(TO_FEET_MULTIPLIER),
            Unit.FEET
        );
    }

    @Override
    protected ValueWithUnit toNauticalMiles(ValueWithUnit value) {
        return new ValueWithUnit(
            value.getValue().multiply(TO_NAUTICAL_MILES_MULTIPLIER),
            Unit.NAUTICAL_MILES
        );
    }
}
