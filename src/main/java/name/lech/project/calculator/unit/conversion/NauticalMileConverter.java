package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
class NauticalMileConverter extends AbstractConverter {

    private static final BigDecimal TO_METERS_MULTIPLIER = new BigDecimal("1852");
    private static final BigDecimal TO_FEET_MULTIPLIER = new BigDecimal("6076.1155");

    @Override
    Unit baseUnitType() {
        return Unit.NAUTICAL_MILES;
    }

    @Override
    protected ValueWithUnit toMeters(ValueWithUnit value) {
        return new ValueWithUnit(
            value.getValue().multiply(TO_METERS_MULTIPLIER),
            Unit.METERS
        );
    }

    @Override
    protected ValueWithUnit toFeet(ValueWithUnit value) {
        return new ValueWithUnit(
            value.getValue().multiply(TO_FEET_MULTIPLIER),
            Unit.FEET
        );
    }

    @Override
    protected ValueWithUnit toNauticalMiles(ValueWithUnit value) {
        return value;
    }
}
