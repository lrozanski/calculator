package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Component
public class UnitConverter {

    private MeterConverter meterConverter;
    private FeetConverter feetConverter;
    private NauticalMileConverter nauticalMileConverter;

    @Autowired
    public UnitConverter(MeterConverter meterConverter, FeetConverter feetConverter, NauticalMileConverter nauticalMileConverter) {
        this.meterConverter = meterConverter;
        this.feetConverter = feetConverter;
        this.nauticalMileConverter = nauticalMileConverter;
    }

    /**
     * Converts a {@link ValueWithUnit} to any supported unit.
     * <p/>
     * Supported units:
     * <ul>
     * <li>{@link Unit#METERS}</li>
     * <li>{@link Unit#FEET}</li>
     * <li>{@link Unit#NAUTICAL_MILES}</li>
     * </ul>
     *
     * @throws NullPointerException          thrown if any of the arguments is null
     * @throws UnsupportedOperationException thrown if an unsupported unit is passed as an argument
     */
    public ValueWithUnit convert(@NotNull ValueWithUnit value, @NotNull Unit toUnit) {
        Objects.requireNonNull(value);
        Objects.requireNonNull(toUnit);

        switch (value.getUnit()) {
            case METERS:
                return meterConverter.toUnit(value, toUnit);
            case FEET:
                return feetConverter.toUnit(value, toUnit);
            case NAUTICAL_MILES:
                return nauticalMileConverter.toUnit(value, toUnit);
            default:
                throw new UnsupportedOperationException();
        }
    }
}
