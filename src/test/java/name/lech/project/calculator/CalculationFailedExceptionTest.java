package name.lech.project.calculator;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculationFailedExceptionTest {

    @Test
    public void unknown() {
        // GIVEN
        String expectedMessage = "Calculation failed due to an unknown error";

        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        CalculationFailedException exception = CalculationFailedException.unknown();

        // THEN
        assertThat(exception).hasMessage(expectedMessage);
        assertThat(exception).hasNoCause();
    }

    @Test
    public void unknown_withCause() {
        // GIVEN
        Throwable cause = new NullPointerException("TEST");
        String expectedMessage = "Calculation failed due to an unknown error";

        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        CalculationFailedException exception = CalculationFailedException.unknown(cause);

        // THEN
        assertThat(exception).hasMessage(expectedMessage);
        assertThat(exception).hasCause(cause);
    }

    @Test
    public void validation() {
        // GIVEN
        Throwable cause = new NullPointerException("TEST");
        String expectedMessage = "Calculation failed due to validation errors";

        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        CalculationFailedException exception = CalculationFailedException.validation(cause);

        // THEN
        assertThat(exception).hasMessage(expectedMessage);
        assertThat(exception).hasCause(cause);
    }

    @Test
    public void unitsNotEqual() {
        // GIVEN
        Throwable cause = new NullPointerException("TEST");
        String expectedMessage = "Cannot perform operations on different units. Convert them to a common unit first.";

        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        CalculationFailedException exception = CalculationFailedException.unitsNotEqual(cause);

        // THEN
        assertThat(exception).hasMessage(expectedMessage);
        assertThat(exception).hasCause(cause);
    }
}
