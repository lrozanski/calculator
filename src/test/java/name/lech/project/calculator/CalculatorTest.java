package name.lech.project.calculator;

import name.lech.project.calculator.operation.*;
import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.UnitValidationException;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Calculator.class)
public class CalculatorTest {

    @MockBean
    private Operations operations;

    @Autowired
    private Calculator calculator;

    @Captor
    private ArgumentCaptor<Operation> operationCaptor = ArgumentCaptor.forClass(Operation.class);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void add() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        calculator.add(Unit.METERS, value);

        // THEN
        verify(operations).performOperation(operationCaptor.capture(), eq(Unit.METERS), eq(value));

        assertThat(operationCaptor.getValue()).isInstanceOf(Addition.class);
    }

    @Test
    public void subtract() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        calculator.subtract(Unit.METERS, value);

        // THEN
        verify(operations).performOperation(operationCaptor.capture(), eq(Unit.METERS), eq(value));

        assertThat(operationCaptor.getValue()).isInstanceOf(Subtraction.class);
    }

    @Test
    public void multiply() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        calculator.multiply(Unit.METERS, value);

        // THEN
        verify(operations).performOperation(operationCaptor.capture(), eq(Unit.METERS), eq(value));

        assertThat(operationCaptor.getValue()).isInstanceOf(Multiplication.class);
    }

    @Test
    public void divide() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        calculator.divide(Unit.METERS, value);

        // THEN
        verify(operations).performOperation(operationCaptor.capture(), eq(Unit.METERS), eq(value));

        assertThat(operationCaptor.getValue()).isInstanceOf(Division.class);
    }

    @Test
    public void add_withUnitValidationException() {
        // GIVEN
        expectedException.expect(CalculationFailedException.class);
        expectedException.expectCause(Matchers.any(UnitValidationException.class));

        // WHEN
        when(operations.performOperation(any(Operation.class), any(Unit.class), any()))
            .thenThrow(UnitValidationException.notEqual(Unit.METERS, Unit.FEET));

        calculator.add(Unit.METERS, new ValueWithUnit(BigDecimal.ONE, Unit.METERS));

        // THEN
        failBecauseExceptionWasNotThrown(CalculationFailedException.class);
    }

    @Test
    public void add_withArithmeticException() {
        // GIVEN
        expectedException.expect(CalculationFailedException.class);
        expectedException.expectCause(Matchers.any(ArithmeticException.class));

        // WHEN
        when(operations.performOperation(any(Operation.class), any(Unit.class), any()))
            .thenThrow(new ArithmeticException());

        calculator.add(Unit.METERS, new ValueWithUnit(BigDecimal.ONE, Unit.METERS));

        // THEN
        failBecauseExceptionWasNotThrown(CalculationFailedException.class);
    }

    @Test
    public void add_withException() {
        // GIVEN
        expectedException.expect(CalculationFailedException.class);
        expectedException.expectCause(Matchers.any(RuntimeException.class));

        // WHEN
        when(operations.performOperation(any(Operation.class), any(Unit.class), any())).thenThrow(new RuntimeException());

        calculator.add(Unit.METERS, new ValueWithUnit(BigDecimal.ONE, Unit.METERS));

        // THEN
        failBecauseExceptionWasNotThrown(CalculationFailedException.class);
    }
}
