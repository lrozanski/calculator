package name.lech.project.calculator;

import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.UnitValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

public class ValueWithUnitTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void requireUnit_withValidUnit() {
        // WHEN
        new ValueWithUnit(BigDecimal.ONE, Unit.METERS).requireUnit(Unit.METERS);
    }

    @Test
    public void requireUnit_withInvalidUnit() {
        // GIVEN
        expectedException.expect(UnitValidationException.class);

        // WHEN
        new ValueWithUnit(BigDecimal.ONE, Unit.METERS).requireUnit(Unit.FEET);

        // THEN
        failBecauseExceptionWasNotThrown(UnitValidationException.class);
    }
}
