package name.lech.project.calculator.cli;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import name.lech.project.calculator.unit.Unit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ArgumentParserTest {

    @Test
    @Parameters({"meters,METERS", "feet,FEET", "nautical miles,NAUTICAL_MILES"})
    public void parseUnit(String unitString, Unit expectedUnit) {
        // WHEN
        Unit result = new ArgumentParser().parseUnit(unitString);

        // THEN
        assertThat(result).isEqualTo(expectedUnit);
    }
}
