package name.lech.project.calculator.cli;

import name.lech.project.calculator.Calculator;
import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CliOperations.class)
public class CliOperationsTest {

    private static final ValueWithUnit FIRST = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);
    private static final ValueWithUnit SECOND = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);
    private static final Unit UNIT = Unit.METERS;

    @MockBean
    private Calculator calculator;

    @Autowired
    private CliOperations cliOperations;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void perform_withAddition() {
        // WHEN
        cliOperations.perform(FIRST, SECOND, "+", UNIT);

        // THEN
        verify(calculator).add(UNIT, FIRST, SECOND);
    }

    @Test
    public void perform_withSubtraction() {
        // WHEN
        cliOperations.perform(FIRST, SECOND, "-", UNIT);

        // THEN
        verify(calculator).subtract(UNIT, FIRST, SECOND);
    }

    @Test
    public void perform_withMultiplication() {
        // GIVEN
        ValueWithUnit FIRST = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);
        ValueWithUnit SECOND = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        cliOperations.perform(FIRST, SECOND, "*", UNIT);

        // THEN
        verify(calculator).multiply(UNIT, FIRST, SECOND);
    }

    @Test
    public void perform_withDivision() {
        // GIVEN
        ValueWithUnit FIRST = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);
        ValueWithUnit SECOND = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        cliOperations.perform(FIRST, SECOND, "/", UNIT);

        // THEN
        verify(calculator).divide(UNIT, FIRST, SECOND);
    }

    @Test
    public void perform_withUnsupportedOperation() {
        // GIVEN
        expectedException.expect(UnsupportedOperationException.class);

        ValueWithUnit FIRST = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);
        ValueWithUnit SECOND = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        cliOperations.perform(FIRST, SECOND, "%", UNIT);

        // THEN
        verifyZeroInteractions(calculator);
        failBecauseExceptionWasNotThrown(UnsupportedOperationException.class);
    }
}
