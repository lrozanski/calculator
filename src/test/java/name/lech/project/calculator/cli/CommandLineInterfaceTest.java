package name.lech.project.calculator.cli;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.assertj.core.groups.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ActiveProfiles("command-line")
@ContextConfiguration(classes = CommandLineInterface.class)
public class CommandLineInterfaceTest {

    @MockBean
    private CliOperations operations;

    @MockBean
    private ArgumentParser argumentParser;

    @MockBean
    private HelpPrinter helpPrinter;

    @Autowired
    private CommandLineInterface commandLineInterface;

    @Test
    public void run() {
        // GIVEN
        ArgumentCaptor<ValueWithUnit> valueCaptor = ArgumentCaptor.forClass(ValueWithUnit.class);

        // WHEN
        when(argumentParser.parseUnit(anyString())).thenReturn(Unit.METERS, Unit.FEET, Unit.NAUTICAL_MILES);

        commandLineInterface.run("1 meters + 10 feet as nautical miles");

        // THEN
        verify(operations).perform(valueCaptor.capture(), valueCaptor.capture(), eq("+"), eq(Unit.NAUTICAL_MILES));

        List<ValueWithUnit> captured = valueCaptor.getAllValues();
        assertThat(captured)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                Tuple.tuple(BigDecimal.ONE, Unit.METERS),
                Tuple.tuple(BigDecimal.TEN, Unit.FEET)
            );
    }

    @Test
    public void run_withDecimalPlaces() {
        // GIVEN
        ArgumentCaptor<ValueWithUnit> valueCaptor = ArgumentCaptor.forClass(ValueWithUnit.class);

        // WHEN
        when(argumentParser.parseUnit(anyString())).thenReturn(Unit.METERS, Unit.FEET, Unit.NAUTICAL_MILES);

        commandLineInterface.run("1.5 meters + 0.10 feet as nautical miles");

        // THEN
        verify(operations).perform(valueCaptor.capture(), valueCaptor.capture(), eq("+"), eq(Unit.NAUTICAL_MILES));

        List<ValueWithUnit> captured = valueCaptor.getAllValues();
        assertThat(captured)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                Tuple.tuple(new BigDecimal("1.5"), Unit.METERS),
                Tuple.tuple(new BigDecimal("0.10"), Unit.FEET)
            );
    }

    @Test
    public void run_withInvalidPattern() {
        // WHEN
        commandLineInterface.run("1 test % 10 feet as nautical miles");

        // THEN
        verify(helpPrinter).print();
    }
}
