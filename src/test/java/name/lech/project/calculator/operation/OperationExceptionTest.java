package name.lech.project.calculator.operation;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OperationExceptionTest {

    @Test
    public void notEnoughValues() {
        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        OperationException exception = OperationException.notEnoughValues();

        // THEN
        assertThat(exception).hasMessage("Not enough values provided");
        assertThat(exception).hasNoCause();
    }
}
