package name.lech.project.calculator.operation;

import name.lech.project.calculator.CalculatorApplication;
import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CalculatorApplication.class)
public class OperationsTest {

    @Autowired
    private Operations operations;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void performOperation_withDifferentUnits() {
        // GIVEN
        ValueWithUnit[] values = new ValueWithUnit[]{
            new ValueWithUnit(BigDecimal.ONE, Unit.METERS),
            new ValueWithUnit(BigDecimal.ONE, Unit.FEET),
            new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES)
        };

        // WHEN
        ValueWithUnit result = operations.performOperation(new Addition(), Unit.METERS, values);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                new BigDecimal("1853.3048"),
                Unit.METERS
            );
    }

    @Test
    public void performOperation_withNullOperation() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        ValueWithUnit[] values = new ValueWithUnit[]{
            new ValueWithUnit(BigDecimal.ONE, Unit.METERS),
            new ValueWithUnit(BigDecimal.ONE, Unit.FEET),
            new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES)
        };

        // WHEN
        operations.performOperation(null, Unit.METERS, values);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void performOperation_withNullUnit() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        ValueWithUnit[] values = new ValueWithUnit[]{
            new ValueWithUnit(BigDecimal.ONE, Unit.METERS),
            new ValueWithUnit(BigDecimal.ONE, Unit.FEET),
            new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES)
        };

        // WHEN
        operations.performOperation(new Addition(), null, values);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void performOperation_withNullValues() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        operations.performOperation(new Addition(), Unit.METERS, (ValueWithUnit[]) null);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void performOperation_withOneValue() {
        // GIVEN
        expectedException.expect(OperationException.class);

        // WHEN
        operations.performOperation(new Addition(), Unit.METERS, new ValueWithUnit(BigDecimal.ONE, Unit.METERS));

        // THEN
        failBecauseExceptionWasNotThrown(OperationException.class);
    }
}
