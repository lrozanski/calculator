package name.lech.project.calculator.operation;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

public class SubtractionTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void perform() {
        // GIVEN
        ValueWithUnit first = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);
        ValueWithUnit other = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        ValueWithUnit result = new Subtraction().perform(first, other);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                BigDecimal.ZERO,
                Unit.METERS
            );
    }

    @Test
    public void perform_withNullFirst() {
        // GIVEN
        expectedException.expect(NullPointerException.class);
        ValueWithUnit other = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        new Subtraction().perform(null, other);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void perform_withNullOther() {
        // GIVEN
        expectedException.expect(NullPointerException.class);
        ValueWithUnit first = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        new Subtraction().perform(first, null);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }
}
