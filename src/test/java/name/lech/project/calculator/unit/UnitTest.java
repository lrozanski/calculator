package name.lech.project.calculator.unit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

public class UnitTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void validateEqual() {
        // WHEN
        Unit.METERS.validateEqual(Unit.METERS);
    }

    @Test
    public void validateEqual_withDifferentUnits() {
        // GIVEN
        expectedException.expect(UnitValidationException.class);

        // WHEN
        Unit.METERS.validateEqual(Unit.FEET);

        // THEN
        failBecauseExceptionWasNotThrown(UnitValidationException.class);
    }

    @Test
    public void validateEqual_withNullUnit() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        Unit.METERS.validateEqual(null);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void requireExpected() {
        // WHEN
        Unit.METERS.requireExpected(Unit.METERS);
    }

    @Test
    public void requireExpected_withDifferentUnits() {
        // GIVEN
        expectedException.expect(UnitValidationException.class);

        // WHEN
        Unit.METERS.requireExpected(Unit.FEET);

        // THEN
        failBecauseExceptionWasNotThrown(UnitValidationException.class);
    }

    @Test
    public void requireExpected_withNullUnit() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        Unit.METERS.requireExpected(null);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }
}
