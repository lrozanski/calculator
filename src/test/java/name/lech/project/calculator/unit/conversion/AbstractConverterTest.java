package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.ValueWithUnit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@RunWith(SpringRunner.class)
public abstract class AbstractConverterTest {

    protected abstract AbstractConverter getConverter();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void toUnit_withNullValue() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        getConverter().toUnit(null, Unit.METERS);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void toUnit_withNullUnit() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        getConverter().toUnit(new ValueWithUnit(BigDecimal.ONE, Unit.METERS), null);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }
}
