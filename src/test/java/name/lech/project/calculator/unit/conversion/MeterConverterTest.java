package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.UnitValidationException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@ContextConfiguration(classes = MeterConverter.class)
public class MeterConverterTest extends AbstractConverterTest {

    private static final BigDecimal TO_FEET_RESULT = new BigDecimal("3.2808399");
    private static final BigDecimal TO_NAUTICAL_MILES_RESULT = new BigDecimal("0.00053995680");

    @Autowired
    private MeterConverter meterConverter;

    @Override
    protected AbstractConverter getConverter() {
        return meterConverter;
    }

    @Test
    public void toUnit_withMeters() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        ValueWithUnit result = meterConverter.toUnit(value, Unit.METERS);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                value.getValue(),
                Unit.METERS
            );
    }

    @Test
    public void toUnit_withFeet() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        ValueWithUnit result = meterConverter.toUnit(value, Unit.FEET);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                TO_FEET_RESULT,
                Unit.FEET
            );
    }

    @Test
    public void toUnit_withNauticalMiles() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        ValueWithUnit result = meterConverter.toUnit(value, Unit.NAUTICAL_MILES);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                TO_NAUTICAL_MILES_RESULT,
                Unit.NAUTICAL_MILES
            );
    }


    @Test
    public void toUnit_withInvalidUnit() {
        // GIVEN
        expectedException.expect(UnitValidationException.class);

        // WHEN
        meterConverter.toUnit(new ValueWithUnit(BigDecimal.ONE, Unit.FEET), Unit.FEET);

        // THEN
        failBecauseExceptionWasNotThrown(UnitValidationException.class);
    }
}
