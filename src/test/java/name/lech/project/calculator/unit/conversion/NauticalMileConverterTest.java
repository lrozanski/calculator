package name.lech.project.calculator.unit.conversion;

import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.UnitValidationException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@ContextConfiguration(classes = NauticalMileConverter.class)
public class NauticalMileConverterTest extends AbstractConverterTest {

    private static final BigDecimal TO_METERS_RESULT = new BigDecimal("1852");
    private static final BigDecimal TO_FEET_RESULT = new BigDecimal("6076.1155");

    @Autowired
    private NauticalMileConverter nauticalMileConverter;

    @Override
    protected AbstractConverter getConverter() {
        return nauticalMileConverter;
    }

    @Test
    public void toUnit_withMeters() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES);

        // WHEN
        ValueWithUnit result = nauticalMileConverter.toUnit(value, Unit.METERS);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                TO_METERS_RESULT,
                Unit.METERS
            );
    }

    @Test
    public void toUnit_withFeet() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES);

        // WHEN
        ValueWithUnit result = nauticalMileConverter.toUnit(value, Unit.FEET);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                TO_FEET_RESULT,
                Unit.FEET
            );
    }

    @Test
    public void toUnit_withNauticalMiles() {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES);

        // WHEN
        ValueWithUnit result = nauticalMileConverter.toUnit(value, Unit.NAUTICAL_MILES);

        // THEN
        assertThat(result)
            .extracting(
                ValueWithUnit::getValue,
                ValueWithUnit::getUnit
            )
            .containsExactly(
                value.getValue(),
                Unit.NAUTICAL_MILES
            );
    }

    @Test
    public void toUnit_withInvalidUnit() {
        // GIVEN
        expectedException.expect(UnitValidationException.class);

        // WHEN
        nauticalMileConverter.toUnit(new ValueWithUnit(BigDecimal.ONE, Unit.METERS), Unit.METERS);

        // THEN
        failBecauseExceptionWasNotThrown(UnitValidationException.class);
    }
}
