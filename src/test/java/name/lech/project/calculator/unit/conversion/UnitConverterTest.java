package name.lech.project.calculator.unit.conversion;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import name.lech.project.calculator.ValueWithUnit;
import name.lech.project.calculator.unit.Unit;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(JUnitParamsRunner.class)
public class UnitConverterTest {

    @Mock
    private MeterConverter meterConverter;

    @Mock
    private FeetConverter feetConverter;

    @Mock
    private NauticalMileConverter nauticalMileConverter;

    @InjectMocks
    private UnitConverter unitConverter;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void convertWithNullValue() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        unitConverter.convert(null, Unit.METERS);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    public void convertWithNullUnit() {
        // GIVEN
        expectedException.expect(NullPointerException.class);

        // WHEN
        unitConverter.convert(new ValueWithUnit(BigDecimal.ONE, Unit.METERS), null);

        // THEN
        failBecauseExceptionWasNotThrown(NullPointerException.class);
    }

    @Test
    @Parameters({"METERS", "FEET", "NAUTICAL_MILES"})
    public void convertMeters(Unit convertTo) {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.METERS);

        // WHEN
        unitConverter.convert(value, convertTo);

        // THEN
        verify(meterConverter).toUnit(value, convertTo);
        verifyZeroInteractions(feetConverter, nauticalMileConverter);
    }

    @Test
    @Parameters({"METERS", "FEET", "NAUTICAL_MILES"})
    public void convertFeet(Unit convertTo) {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.FEET);

        // WHEN
        unitConverter.convert(value, convertTo);

        // THEN
        verify(feetConverter).toUnit(value, convertTo);
        verifyZeroInteractions(meterConverter, nauticalMileConverter);
    }

    @Test
    @Parameters({"METERS", "FEET", "NAUTICAL_MILES"})
    public void convertNauticalMiles(Unit convertTo) {
        // GIVEN
        ValueWithUnit value = new ValueWithUnit(BigDecimal.ONE, Unit.NAUTICAL_MILES);

        // WHEN
        unitConverter.convert(value, convertTo);

        // THEN
        verify(nauticalMileConverter).toUnit(value, convertTo);
        verifyZeroInteractions(meterConverter, feetConverter);
    }
}
