package name.lech.project.calculator.unit.validation;

import name.lech.project.calculator.unit.Unit;
import name.lech.project.calculator.unit.UnitValidationException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UnitValidationExceptionTest {

    @Test
    public void notEqual() {
        // GIVEN
        String expectedMessage = String.format("Units %s and %s are not equal", Unit.METERS.name(), Unit.FEET.name());

        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        UnitValidationException exception = UnitValidationException.notEqual(Unit.METERS, Unit.FEET);

        // THEN
        assertThat(exception).hasMessage(expectedMessage);
    }

    @Test
    public void unexpected() {
        // GIVEN
        String expectedMessage = String.format("Unit %s is different from expected: %s", Unit.METERS.name(), Unit.FEET.name());

        // WHEN
        @SuppressWarnings("ThrowableNotThrown")
        UnitValidationException exception = UnitValidationException.unexpected(Unit.METERS, Unit.FEET);

        // THEN
        assertThat(exception).hasMessage(expectedMessage);
    }
}
